<?php

use \NetPeak\TimeLine\Core\Response;
use \NetPeak\TimeLine\Core\Request;
use \NetPeak\TimeLine\Core\Dispatcher;
use \NetPeak\TimeLine\Core\Router;
use \NetPeak\TimeLine\Core\Route;
use \NetPeak\TimeLine\Core\AppKernel;
use \NetPeak\TimeLine\Exception\NotFoundException;

$response = new Response();

$response->addHeader('Content-Type: application/json; charset=UTF-8');
$response->addHeader('Access-Control-Allow-Origin: *');

try {
    if (!file_exists(__DIR__.'/config/parameters.ini')) {
        throw  new \Exception('App critical error: file \'parameters.ini\' doesn\'t exist.');
    }

    $parameters = parse_ini_file(__DIR__.'/config/parameters.ini', true, INI_SCANNER_TYPED);
    $parameters['cache_location'] = dirname(__DIR__).'/var/cache/';

    $routeList = [
        new Route("/api/timeline/twitter", "NetPeak\\TimeLine\\Controller\\TwitterTimeLineController"),
    ];
    $router = new Router($routeList);
    $app    = new AppKernel($router, new Dispatcher(), $parameters);

    $app->run(new Request($_SERVER['REQUEST_URI']), $response);

} catch (NotFoundException $e) {
    error_log($e->getMessage());
    $response
        ->setStatusCode(Response::HTTP_STATUS_CODE_NOT_FOUND)
        ->send(json_encode([
            'message' => $e->getMessage()
        ]));
} catch (\Exception $e) {
    error_log($e->getMessage());
    $response
        ->setStatusCode(Response::HTTP_STATUS_CODE_INTERNAL_ERROR)
        ->send(json_encode([
            'message' => $e->getMessage()
        ]));
}


