<?php

namespace NetPeak\TimeLine\Core;

use NetPeak\TimeLine\Controller\ControllerInterface;

class Route
{
    /** @var string */
    private $controllerClass;

    /** @var string */
    protected $path;

    /**
     * Route constructor.
     *
     * @param string $path
     * @param string $controllerClass
     */
    public function __construct($path, $controllerClass)
    {
        $this->path = $path;
        $this->controllerClass = $controllerClass;
    }

    /**
     * @param RequestInterface $request
     *
     * @return bool
     */
    public function match(RequestInterface $request)
    {
        return $this->path === $request->getUri();
    }

    /**
     * @param array $parameters
     *
     * @return ControllerInterface
     */
    public function createController($parameters = [])
    {
        return new $this->controllerClass($parameters);
    }
}
