<?php

namespace NetPeak\TimeLine\Core;

interface ResponseInterface
{
    /**
     * @param string $header
     *
     * @return ResponseInterface
     */
    public function addHeader($header);

    /**
     * @param array $headers
     *
     * @return ResponseInterface
     */
    public function addHeaders(array $headers);

    /**
     * @return array
     */
    public function getHeaders();

    /**
     * @param $statusCode
     *
     * @return ResponseInterface
     */
    public function setStatusCode($statusCode);

    /**
     * @param $outputData
     *
     * @return mixed
     */
    public function send($outputData);
}
