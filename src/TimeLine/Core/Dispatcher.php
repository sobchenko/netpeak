<?php

namespace NetPeak\TimeLine\Core;

class Dispatcher
{
    /**
     * @param Route $route
     * @param RequestInterface $request
     * @param ResponseInterface $response
     * @param array $parameters
     */
    public function dispatch(
        Route $route,
        RequestInterface $request,
        ResponseInterface $response,
        array $parameters = []
    ) {
        //TODO Necessary to Improve code bellow:
        $hasNoCache = true;
        if (isset($parameters['system']['cache_ttl']) && $parameters['system']['cache_ttl']) {
            $cacheFile = $parameters['cache_location'].'cached'.str_replace('/', '_', $request->getUri()).'.json';
            if (file_exists($cacheFile) && time() - $parameters['system']['cache_ttl'] < filemtime($cacheFile)) {
                $response->addHeader("Cache-Control: max-age={$parameters['system']['cache_ttl']}");
                $content = file_get_contents($cacheFile);
                $response->send($content);
                $hasNoCache = false;
            }
        }

        if ($hasNoCache) {
            ob_start();

            $response->addHeader('Cache-Control: no-cache');
            $controller = $route->createController($parameters);
            $controller->executeAction($request, $response);

            if (isset($cacheFile)) {
                $cached = fopen($cacheFile, 'w');
                fwrite($cached, ob_get_contents());
                fclose($cached);
            }

            ob_end_flush();
        }
    }
}
