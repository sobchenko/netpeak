<?php

namespace NetPeak\TimeLine\Core;

interface RequestInterface
{
    /**
     * @return string
     */
    public function getUri();

    /**
     * @param $key
     * @param $value
     *
     * @return RequestInterface
     */
    public function setParam($key, $value);

    /**
     * @param $key
     *
     * @return mixed
     */
    public function getParam($key);

    /**
     * @return array
     */
    public function getParams();
}
