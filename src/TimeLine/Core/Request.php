<?php

namespace NetPeak\TimeLine\Core;

class Request implements RequestInterface
{
    /**
     * @var array
     */
    protected $params;

    /**
     * @var string
     */
    protected $uri;

    /**
     * Request constructor.
     * @param string $uri
     * @param array $params
     */
    public function __construct($uri, array $params = [])
    {
        $this->uri = $uri;
        $this->params = $params;
    }

    /**
     * @return string
     */
    public function getUri()
    {
        return $this->uri;
    }

    /**
     * @inheritdoc
     */
    public function setParam($key, $value)
    {
        $this->params[$key] = $value;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getParam($key)
    {
        if (!isset($this->params[$key])) {
            throw new \InvalidArgumentException("The request parameter with key '$key' is invalid.");
        }
        return $this->params[$key];
    }

    /**
     * @inheritdoc
     */
    public function getParams()
    {
        return $this->params;
    }
}
