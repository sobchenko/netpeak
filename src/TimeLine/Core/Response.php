<?php

namespace NetPeak\TimeLine\Core;

class Response implements ResponseInterface
{
    const HTTP_STATUS_CODE_OK             = 200;
    const HTTP_STATUS_CODE_BAD_REQUEST    = 400;
    const HTTP_STATUS_CODE_NOT_FOUND      = 404;
    const HTTP_STATUS_CODE_INTERNAL_ERROR = 500;

    /** @var  array */
    protected $headers = [];

    /**
     * @inheritdoc
     */
    public function addHeader($header)
    {
        $this->headers[] = $header;
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function addHeaders(array $headers)
    {
        foreach ($headers as $header) {
            $this->addHeader($header);
        }
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function getHeaders()
    {
        return $this->headers;
    }

    /**
     * @inheritdoc
     */
    public function setStatusCode($statusCode)
    {
        http_response_code($statusCode);
        return $this;
    }

    /**
     * @inheritdoc
     */
    public function send($responseData = null)
    {
        if (!headers_sent()) {
            foreach ($this->headers as $header) {
                header("$header", true);
            }
        }
        echo $responseData;
    }
}
