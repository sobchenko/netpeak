<?php

namespace NetPeak\TimeLine\Core;

use NetPeak\TimeLine\Exception\NotFoundException;

class Router
{
    /** @var Route[] */
    protected $routes;

    /**
     * Router constructor.
     *
     * @param Route[] $routes
     */
    public function __construct(array $routes = [])
    {
        $this->addRoutes($routes);
    }

    /**
     * @param Route $route
     *
     * @return $this
     */
    public function addRoute(Route $route)
    {
        $this->routes[] = $route;
        return $this;
    }

    /**
     * @param Route[] $routes
     *
     * @return $this
     */
    public function addRoutes(array $routes)
    {
        foreach ($routes as $route) {
            $this->addRoute($route);
        }
        return $this;
    }

    /**
     * @return Route[]
     */
    public function getRoutes()
    {
        return $this->routes;
    }

    /**
     * @param RequestInterface $request
     *
     * @return Route
     * @throws NotFoundException
     */
    public function route(RequestInterface $request)
    {
        /** @var Route $route */
        foreach ($this->routes as $route) {
            if ($route->match($request)) {
                return $route;
            }
        }
        throw new NotFoundException("No route matched the given URI: {$request->getUri()}");
    }
}
