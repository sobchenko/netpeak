<?php

namespace NetPeak\TimeLine\Core;

class AppKernel
{
    /** @var  Router */
    protected $router;

    /** @var Dispatcher */
    protected $dispatcher;

    /** @var array */
    protected $parameters;

    /**
     * AppKernel constructor.
     * @param Router $router
     * @param Dispatcher $dispatcher
     * @param array $parameters
     */
    public function __construct(Router $router, Dispatcher $dispatcher, array $parameters = [])
    {
        $this->router = $router;
        $this->dispatcher = $dispatcher;
        $this->parameters = $parameters;
    }

    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     */
    public function run(RequestInterface $request, ResponseInterface $response)
    {
        $route = $this->router->route($request);
        $this->dispatcher->dispatch($route, $request, $response, $this->parameters);
    }
}
