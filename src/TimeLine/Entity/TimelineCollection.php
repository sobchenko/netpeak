<?php

namespace NetPeak\TimeLine\Entity;

class TimelineCollection implements CollectionInterface
{
    /** @var  TimelineUnit[] */
    private $timeline;

    /**
     * @param TimelineUnit $unit
     *
     * @return $this
     */
    public function addTimelineUnit(TimelineUnit $unit)
    {
        $this->timeline[] = $unit;
        return $this;
    }

    /**
     * @return TimelineUnit[]
     */
    public function getTimeline()
    {
        return $this->timeline;
    }
}
