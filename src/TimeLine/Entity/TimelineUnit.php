<?php

namespace NetPeak\TimeLine\Entity;

class TimelineUnit implements \JsonSerializable
{

    /** @var int */
    public $id;

    /** @var int */
    public $republished_count;

    /** @var int */
    public $favorite_count;

    /** @var string */
    public $news_text;

    /** @var string */
    public $news_url;

    /** @var string */
    public $publisher_logo;

    /** @var string */
    public $published_ago;

    /** @var string */
    public $publisher_name;

    /** @var string */
    public $publisher_url;

    /** @var string */
    public $publisher_timeline_name;


    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @param int $counter
     */
    public function setRepublishedCount($counter)
    {
        $this->republished_count = $counter;
    }

    /**
     * @param int $counter
     */
    public function setFavoriteCount($counter)
    {
        $this->favorite_count = $counter;
    }

    /**
     * @param string $text
     */
    public function setNewsText($text)
    {
        $this->news_text = $text;
    }

    /**
     * @param string $text
     */
    public function setNewsUrl($text)
    {
        $this->news_url = $text;
    }

    /**
     * @param string $text
     */
    public function setPublisherLogo($text)
    {
        $this->publisher_logo = $text;
    }

    /**
     * @param string $text
     */
    public function setPublishedAgo($text)
    {
        $this->published_ago = $text;
    }

    /**
     * @param string $text
     */
    public function setPublisherName($text)
    {
        $this->publisher_name = $text;
    }

    /**
     * @param string $text
     */
    public function setPublisherUrl($text)
    {
        $this->publisher_url = $text;
    }

    /**
     * @param string $text
     */
    public function setPublisherTimelineName($text)
    {
        $this->publisher_timeline_name = $text;
    }

    /**
     * @return array
     */
    public function jsonSerialize()
    {
        return get_object_vars($this);
    }
}
