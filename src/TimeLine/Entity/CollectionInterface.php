<?php

namespace NetPeak\TimeLine\Entity;

interface CollectionInterface
{
    public function getTimeline();
}
