<?php

namespace NetPeak\TimeLine\Controller;

use NetPeak\TimeLine\Core\Response;
use NetPeak\TimeLine\Core\ResponseInterface;

abstract class AbstractJsonController implements ControllerInterface
{
    /** @var array */
    protected $parameters;

    /**
     * AbstractJsonController constructor.
     *
     * @param array $parameters
     */
    public function __construct($parameters = [])
    {
        $this->parameters = $parameters;
    }

    /**
     * @param ResponseInterface $response
     * @param array $data
     * @param int $statusCode

     * @return void
     */
    protected function createSuccessResponse(
        ResponseInterface $response,
        array $data = [],
        $statusCode = Response::HTTP_STATUS_CODE_OK
    ) {
        $response->setStatusCode($statusCode);
        $response->send(json_encode($data));
    }

    /**
     * @param ResponseInterface $response
     * @param \Exception $e
     * @param int $statusCode
     *
     * @return void
     */
    protected function createFailedResponse(
        ResponseInterface $response,
        \Exception $e,
        $statusCode = Response::HTTP_STATUS_CODE_INTERNAL_ERROR
    ) {
        $response->setStatusCode($statusCode);
        $response->send(json_encode([
            'message' => $e->getMessage(),
            'code' => $statusCode
        ]));
        error_log($e->getMessage());
    }
}
