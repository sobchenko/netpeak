<?php

namespace NetPeak\TimeLine\Controller;

use NetPeak\TimeLine\Core\RequestInterface;
use NetPeak\TimeLine\Core\ResponseInterface;

interface ControllerInterface
{
    /**
     * @param RequestInterface $request
     * @param ResponseInterface $response
     *
     * @return mixed
     */
    public function executeAction(RequestInterface $request, ResponseInterface $response);
}
