<?php

namespace NetPeak\TimeLine\Controller;

use NetPeak\TimeLine\Core\RequestInterface;
use NetPeak\TimeLine\Core\ResponseInterface;
use NetPeak\TimeLine\Service\FetchTimeline;

class TwitterTimeLineController extends AbstractJsonController
{
    /** @inheritdoc */
    public function executeAction(RequestInterface $request, ResponseInterface $response)
    {
        try {
            $timelineService = new FetchTimeline(FetchTimeline::TWITTER_TIMELINE, $this->parameters);
            $data = $timelineService->getTimeline();
            $this->createSuccessResponse($response, $data);
        } catch (\Exception $e) {
            $this->createFailedResponse($response, $e);
        }
    }
}
