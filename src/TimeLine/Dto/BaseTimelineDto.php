<?php

namespace NetPeak\TimeLine\Dto;

class BaseTimelineDto
{
    /**
     * @param array $data
     * @param string $key
     * @param mixed $default
     * @return mixed
     */
    protected function getValue(array $data, $key, $default = null)
    {
        return isset($data[$key]) ? $data[$key] : $default;
    }

    /**
     * @param \DateTime $dateTime
     * @return string
     */
    protected function getHumanReadableTimeAgo(\DateTime $dateTime)
    {
        $nowDateTime = new \DateTime('now');
        $interval = $nowDateTime->diff($dateTime);
        if ($interval->y) {
            return $this->getPeriodPreparedString($interval->y, 'year');
        }
        if ($interval->m) {
            return $this->getPeriodPreparedString($interval->m, 'month');
        }

        $timeStampDifference = $nowDateTime->getTimestamp() - $dateTime->getTimestamp();
        $timeStampDifference = ($timeStampDifference < 1) ? 1 : $timeStampDifference;
        $periods = array(
            604800 => 'week',
            86400 => 'day',
            3600 => 'hour',
            60 => 'minute',
            1 => 'second'
        );

        foreach ($periods as $unit => $periodName) {
            if ($timeStampDifference < $unit) {
                continue;
            }
            $numberOfUnits = floor($timeStampDifference / $unit);
            return $this->getPeriodPreparedString($numberOfUnits, $periodName);
        }
    }

    /**
     * @param integer $numberOfUnits
     * @param string $periodName
     * @return string
     */
    private function getPeriodPreparedString($numberOfUnits, $periodName)
    {
        return $numberOfUnits.' '.$periodName.(($numberOfUnits > 1) ? 's' : '');
    }
}
