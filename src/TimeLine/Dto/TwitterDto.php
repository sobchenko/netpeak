<?php

namespace NetPeak\TimeLine\Dto;

class TwitterDto extends BaseTimelineDto
{
    /** @var int */
    private $id;

    /** @var int */
    private $republishedCount;

    /** @var int */
    private $favoriteCount;

    /** @var string */
    private $newsText;

    /** @var string */
    private $publisherLogo;

    /** @var string */
    private $publisherName;

    /** @var string */
    private $publisherUrl;

    /** @var string */
    private $publisherTimelineName;

    /** @var bool */
    private $truncated;

    /** @var array */
    private $entityData;

    /** @var string */
    private $createdAt;

    public function __construct(array $data)
    {
        $this->truncated = (bool) $this->getValue($data, 'truncated', false);
        $this->id = (int) $this->getValue($data, 'id');
        $this->entityData = $this->setEntityData($data);
        $this->republishedCount = (int) $this->getValue($this->entityData, 'retweet_count');
        $this->favoriteCount = (int) $this->getValue($this->entityData, 'favorite_count');
        $this->newsText = $this->getValue($this->entityData, 'text');
        $this->createdAt = $this->getValue($this->entityData, 'created_at');
        $this->publisherLogo = $this->getValue($this->entityData['user'], 'profile_image_url');
        $this->publisherName = $this->getValue($this->entityData['user'], 'name');
        $this->publisherTimelineName = '@'.$this->getValue($this->entityData['user'], 'screen_name');
        $this->publisherUrl = 'https://twitter.com/'.$this->getValue($this->entityData['user'], 'screen_name');
    }

    /**
     * @return string
     */
    public function getPublishedAgo()
    {
        return $this->getHumanReadableTimeAgo(new \DateTime($this->createdAt));
    }

    /**
     * @return null
     */
    public function getNewsUrl()
    {
        $timelineUrl = $this->entityData['entities']['urls'][0]['url'];
        if (isset($timelineUrl)) {
            return $timelineUrl;
        }
        $timelineUrl = $this->entityData['entities']['media'][0]['url'];
        return isset($timelineUrl) ? $timelineUrl : null;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getRepublishedCount()
    {
        return $this->republishedCount;
    }

    /**
     * @return int
     */
    public function getFavoriteCount()
    {
        return $this->favoriteCount;
    }

    /**
     * @return string
     */
    public function getNewsText()
    {
        return $this->makeUrlClickable($this->newsText);
    }

    /**
     * @return string
     */
    public function getPublisherLogo()
    {
        return $this->publisherLogo;
    }

    /**
     * @return string
     */
    public function getPublisherName()
    {
        return $this->publisherName;
    }

    /**
     * @return string
     */
    public function getPublisherUrl()
    {
        return $this->publisherUrl;
    }

    /**
     * @return string
     */
    public function getPublisherTimelineName()
    {
        return $this->publisherTimelineName;
    }

    /**
     * @return array
     */
    private function setEntityData(array $data)
    {
        if (!isset($data['retweeted_status'])) {
            return $data;
        }
        return $this->truncated ? $data : $data['retweeted_status'];
    }

    protected function makeUrlClickable($text)
    {
        return preg_replace('!(((f|ht)tp(s)?://)[-a-zA-Zа-яА-Я()0-9@:%_+.~#?&;//=]+)!i', '<a href="$1">$1</a>', $text);
    }
}
