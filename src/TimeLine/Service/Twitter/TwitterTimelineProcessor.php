<?php

namespace NetPeak\TimeLine\Service\Twitter;

use NetPeak\TimeLine\Dto\TwitterDto;
use NetPeak\TimeLine\Entity\TimelineCollection;
use NetPeak\TimeLine\Entity\TimelineUnit;
use NetPeak\TimeLine\Service\TimelineProcessorInterface;

class TwitterTimelineProcessor implements TimelineProcessorInterface
{
    /** @var  array */
    private $parameters;

    /** @var TimelineCollection */
    private $timelineCollection;

    /** @var  TwitterRowTimelineCollector */
    private $twitterDataCollector;

    /**
     * TwitterTimelineProcessor constructor.
     * @param array $parameters
     */
    public function __construct($parameters = [])
    {
        $this->parameters = $parameters;
        $this->initTwitterDataCollector();
        $this->timelineCollection = new TimelineCollection();
    }

    /**
     * @inheritdoc
     */
    public function getTimelineCollection()
    {
        $this->createTimelineFromRowData();
        return $this->timelineCollection;
    }

    protected function createTimelineFromRowData()
    {
        $rowData = json_decode($this->twitterDataCollector->getRowTimeline(), true);
        foreach ($rowData as $unit) {
            $twitterDTO = new TwitterDto($unit);
            $timelineUnit = new TimelineUnit();
            $timelineUnit->setId($twitterDTO->getId());
            $timelineUnit->setNewsUrl($twitterDTO->getNewsUrl());
            $timelineUnit->setNewsText($twitterDTO->getNewsText());
            $timelineUnit->setPublisherUrl($twitterDTO->getPublisherUrl());
            $timelineUnit->setPublisherLogo($twitterDTO->getPublisherLogo());
            $timelineUnit->setPublisherName($twitterDTO->getPublisherName());
            $timelineUnit->setPublishedAgo($twitterDTO->getPublishedAgo());
            $timelineUnit->setPublisherTimelineName($twitterDTO->getPublisherTimelineName());
            $timelineUnit->setRepublishedCount($twitterDTO->getRepublishedCount());
            $timelineUnit->setFavoriteCount($twitterDTO->getFavoriteCount());

            $this->timelineCollection->addTimelineUnit($timelineUnit);
        }
        return $rowData;
    }

    /**
     * Initialize Twitter Row Data Collector
     */
    protected function initTwitterDataCollector()
    {
        $this->twitterDataCollector = new TwitterRowTimelineCollector(
            $this->getParemeter('consumer_key'),
            $this->getParemeter('consumer_secret'),
            $this->getParemeter('access_token'),
            $this->getParemeter('access_token_secret')
        );

        $this->twitterDataCollector
            ->setTwittsNumber($this->getParemeter('twits_number'))
            ->setScreenName($this->getParemeter('screen_name'))
            ->setApiUrl($this->getParemeter('twitter_api'));
    }

    /**
     * @param $key
     *
     * @return mixed
     */
    protected function getParemeter($key)
    {
        if (is_null($this->parameters[$key])) {
            throw new \InvalidArgumentException("Parameters section for twitter has no key: [$key]");
        }
        return $this->parameters[$key];
    }
}
