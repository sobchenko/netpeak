<?php

namespace NetPeak\TimeLine\Service\Twitter;

class TwitterRowTimelineCollector
{
    /** @var string */
    private $consumerKey;

    /** @var string */
    private $consumerSecret;

    /** @var string */
    private $accessToken;

    /** @var string */
    private $accessTokenSecret;

    /** @var int */
    private $twitsNumber = 10;

    /** @var string */
    private $screenName;

    /** @var string */
    private $apiUrl;

    /** @var \OAuth */
    private $oauth;

    /**
     * TwitterRowTimelineCollector constructor.
     *
     * @param string $consumerKey
     * @param string $consumerSecret
     * @param string $accessToken
     * @param string $accessTokenSecret
     */
    public function __construct($consumerKey, $consumerSecret, $accessToken = null, $accessTokenSecret = null)
    {
        $this->consumerKey = $consumerKey;
        $this->consumerSecret = $consumerSecret;
        $this->accessToken = $accessToken;
        $this->accessTokenSecret = $accessTokenSecret;

        $this->oauth = new \OAuth(
            $this->consumerKey,
            $this->consumerSecret,
            OAUTH_SIG_METHOD_HMACSHA1,
            OAUTH_AUTH_TYPE_AUTHORIZATION
        );

        $this->setToken($this->accessToken, $this->accessTokenSecret);
    }

    /**
     * @return string
     * @throws \Exception
     */
    public function getRowTimeline()
    {
        $url = $this->apiUrl.'/statuses/user_timeline.json?count='.$this->twitsNumber.'&screen_name='.$this->screenName;
        try {
            $this->oauth->fetch($url);
            return $this->oauth->getLastResponse();
        } catch (\OAuthException $E) {
            throw new \Exception($E->lastResponse);
        }
    }

    /**
     * @param string $accessToken
     * @param string $accessTokenSecret
     *
     * @return $this
     */
    public function setToken($accessToken, $accessTokenSecret)
    {
        $this->oauth->setToken($accessToken, $accessTokenSecret);
        return $this;
    }

    /**
     * @param string $url
     *
     * @return $this
     */
    public function setApiUrl($url)
    {
        $this->apiUrl = $url;
        return $this;
    }

    /**
     * @param string $screenName
     *
     * @return $this
     */
    public function setScreenName($screenName)
    {
        $this->screenName = $screenName;
        return $this;
    }

    /**
     * @param int $twitsNumber
     *
     * @return $this
     */
    public function setTwittsNumber($twitsNumber)
    {
        $this->twitsNumber = $twitsNumber;
        return $this;
    }
}
