<?php

namespace NetPeak\TimeLine\Service;

use NetPeak\TimeLine\Entity\TimelineCollection;

interface TimelineProcessorInterface
{

    /**
     * @return TimelineCollection
     */
    public function getTimelineCollection();
}
