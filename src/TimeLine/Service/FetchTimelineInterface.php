<?php

namespace NetPeak\TimeLine\Service;

use NetPeak\TimeLine\Entity\TimelineUnit;

interface FetchTimelineInterface
{
    /**
     * @return TimelineUnit[]
     */
    public function getTimeline();
}
