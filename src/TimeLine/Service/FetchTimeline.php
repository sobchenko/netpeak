<?php

namespace NetPeak\TimeLine\Service;

use NetPeak\TimeLine\Entity\TimelineUnit;
use NetPeak\TimeLine\Service\Twitter\TwitterTimelineProcessor;

class FetchTimeline implements FetchTimelineInterface
{
    const TWITTER_TIMELINE = 'twitter_timeline';

    /**
     * @var TimelineProcessorInterface
     */
    private $timelineProcessor;

    public function __construct($timelineName, $parameters)
    {
        switch ($timelineName) {
            case self::TWITTER_TIMELINE:
                $this->validateParameters($parameters, self::TWITTER_TIMELINE);
                $this->timelineProcessor = new TwitterTimelineProcessor($parameters[$timelineName]);
                break;
            default:
                throw new \LogicException('Not implemented logic for '.$timelineName);
        }
    }

    /**
     * @return TimelineUnit[]
     */
    public function getTimeline()
    {
        $result = $this->timelineProcessor->getTimelineCollection()->getTimeline();
        return $result;
    }

    /**
     * @param array $parameters
     *
     * @param string $key
     */
    protected function validateParameters($parameters = [], $key)
    {
        if (!isset($parameters[$key])) {
            throw new \InvalidArgumentException("Parameters has no section: [$key]");
        }
    }
}
